#!/bin/sh

BUILD_PATH="build"

SITE_PACKAGES=`python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"`

if [ -d "$BUILD_PATH" ]; then
    echo "already build, ignore."
else
    echo "building $PWD"
    python setup.py build
fi 

cp -Rf build/lib/* $SITE_PACKAGES
cp -Rf *.egg-info $SITE_PACKAGES
