# -*- coding:utf-8 -*-
__author__ = 'Forcs'


class ConstClass(object):

    class ConstError(TypeError):
        pass

    def __setattr__(self, key, value):
        if self.__dict__.has_key(key):
            raise self.ConstError, u'%s.%s 为常量，不能再修改' % (self.__class__.__name__, key)

        self.__dict__[key] = value
