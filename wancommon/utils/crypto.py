# -*- coding:utf8 -*-

import pyDes
from django.conf import settings

class Des(object):

    def __init__(self, key=None):
        if not key:
            self._sk = getattr(settings, 'SECRET_KEY', None)
        else:
            self._sk = key