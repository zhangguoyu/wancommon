# -*- coding:utf8 -*-
from django.http import HttpResponse
from result_code import ResultCode

import json


def send_response_with_result(result):
    """
    响应结果
    :param result:
    :return:
    """
    return HttpResponse(result, content_type="application/json;charset=utf-8")


def send_ok_response_with_data(data=None, rc=ResultCode.OK, message=u''):
    res = {
        "resultCode": rc,
        "message": message,
    }
    if data is not None:
        res["data"] = data

    return send_response_with_result(json.dumps(res))


def send_ok_response_with_list(list, rc=ResultCode.OK, message=u''):
    return send_ok_response_with_data(data={
        "list": list
    }, rc=rc, message=message)


def send_error_response_on_exception(exception=None):
    return send_error_response_with_message(message=u'%s' % exception if exception else u'')


def send_error_response_with_message(message=u'', rc=ResultCode.ERROR):
    res = {
        "resultCode": rc,
        "message": message,
    }
    return send_response_with_result(json.dumps(res))
