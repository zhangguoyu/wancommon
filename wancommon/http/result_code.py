# -*- coding:utf-8 -*-

from common.utils import const_classes


class ResultCode(const_classes.ConstClass):

    OK = 1  # 表示返回的结果正常
    ERROR = 0  # 表示后台处理发生异常
