# -*- coding:utf8 -*-
from setuptools import setup, find_packages

import wancommon

setup(
    name='wancommon',
    version=wancommon.__version__,
    author='Zhang Guoyu',
    author_email='zhang.guoyu@wanbatv.com',
    description='A common library for Wanba',
    long_description=open('README.md').read(),
    url='http://github.com/zhangguoyu/wancommon',
    zip_safe=False,
    include_package_data=True,
    packages=find_packages(),
    install_requires=["django>=1.7.0"],
)
